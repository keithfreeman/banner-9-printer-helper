<?php

if (!function_exists('cacheBustingUrl')) {

    /**
     * @param $path
     * @param null $dependency
     * @return string
     */
    function cacheBustingUrl($path, $dependency = null)
    {
        $url = url($path);

        $dependencyModTime = file_exists($dependency) ? filemtime($dependency) : 0;

        return !file_exists(public_path($path))
            ? $url
            : $url . '?ver=' . max(filemtime(public_path($path)), $dependencyModTime);
    }
}

if (!function_exists('greatestModTime')) {

    /**
     * @param $path
     * @return int
     */
    function greatestModTime($path)
    {
        return trim(`find $path -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -d" " -f1`);
    }
}

if (!function_exists('processTemplate')) {

    /**
     * @param $view
     * @return string
     */
    function processTemplate($view)
    {
        return !view()->exists($view)
            ? ''
            : substr(json_encode(view($view)->render()), 1, -1);
    }
}