# Banner 9 Printer Helper App

This application works in conjunction with a browser extension to simplify Banner 9 forms for friendly printing.

# Installation

This is a basic Laravel application and does not have many requirements.

1) `composer install`
2) `cp .env.example .env`
3) `php artisan key:generate`
3) Configure `.env`
    * APP_ENV - production or local (or maybe staging if you have one)
    * APP_DEBUG - true or false
    * APP_URL - as appropriate
    * APP_TIME_ZONE - must be a valid PHP time zone value
    * ERROR_EMAIL - user reported issues are sent to this address
    * DB_CONNECTION - leave unchanged for sqlite, or whatever supported type you'd like to use
    * MAIL_* - settings as necessary to deliver user error reports
4) If using sqlite database, `touch storage/database.sqlite`
5) `php artisan migrate`
6) `php artisan add:user`
7) If necessary, make sure the web service user has recursive write access to `bootstrap/cache/`, `storage/`, and the sqlite database.

# Configuration

Much of the configuration relates to how things are presented in the UI.  The top bar on each page has two color configurations.  It will change colors based on whether the application instance is in production or not.  The environment setting is determined by the `APP_ENV` value found in the `.env` file discussed under **Installation** above.  The default production color is a nice green.  It can be changed by editing the below line in the `public/css/application.css` file:
  
  ```
  .navbar-default {
      background-color: #0c5449;
  }
```

In a non-production environment, the color defaults to a nice reddish color.  More on changing that value can be found next.

Aside of the above setting, all of the UI configuration can be done through a configuration file at `config/white-label-ui.php`.  This file contains a few important values.

```
<?php

// define some basic settings for the UI
return [

    // relative url for the top nav logo and application name
    'app-home' => '/',

    // default title text for browser tab
    'app-title' => 'Banner 9 Printer Helper',

    // absolute url to the Chrome extension to be used with this application
    'chrome-extension' => 'https://chrome.google.com/webstore/',

    // relative url to the favicon
    'favicon' => '/white-label-ui/img/favicon.ico',

    // relative url to the small logo - 30px height max
    'logo' => '/white-label-ui/img/white-label-ui-logo-small.png',
    
    // relative url to the medium logo - 48px height max
    'logo' => '/white-label-ui/img/white-label-ui-logo-medium.png',

    // non-production settings for the top bar 
    'top-bar-non-production-main' => '#8B2145',
    'top-bar-non-production-border' => '#671931',

    // whether or not to display a page header
    'use-page-header' => true,
];
```

The relevant ones are as follows:

* **app-title**: This will be displayed as the title for the browser tab.  To set the text for the top bar change the _name_ value in `config/app.php`.
* **chrome-extension**: After publishing your extension to the Chrome App Store, replace this value with the full URL to the extension in the store.
* **favicon**: The included favicon is simply a transparent image.  Replace with one of your choosing by putting it somewhere in the `public/` folder and updating the relative URL here.
* **logo**: The included logo is simply a transparent image.  Replace with one of your choosing by putting it somewhere in the `public/` folder and updating the relative URL here.
* **top-bar-non-production-main**: This will set the color of the top bar in a non-production environment.  See above about setting the color for production.
* **top-bar-non-production-border**: This will set the border color of the top bar in a non-production environment.  There is no border for production, but if you wanted to add one you could do that in the CSS file as described above using the `.navbar-default` selector.

# Sample Pages

In a non-production instance, there are two sample pages that can be viewed to see how the application reacts and adapts with Banner 9 forms markup.  The two pages showcase the two types of display format that Banner 9 forms use: grid and presentation.
 
 * **STVTERM**: Go to `<application url>/test/grid` to look at an STVTERM form with a grid layout.
 * **PTRBDCA**: Go to `<application url>/test/presentation` to look at a PTRBDCA form for a Federal Tax deduction with a presentation layout.
 
 # A Final Caution
 
 This application works by parsing HTML markup from Banner 9 forms.  As such, it is very dependent on the precise format of that markup.  All possible care has been taken to assure that the various different ways that data and labels are written will be handled with accuracy and thoroughness.  However, it is still possible that something has been missed.  Likely culprits for something being parsed incorrectly, or just missing in general, are form elements that are currently highlighted or selected on the Banner 9 form. Additionally, if at some point the markup that Banner 9 uses changes, perhaps even slightly, this entire application may fail to work and will require changes to the parsing logic.  We can hope that should this ever happen, native Banner 9 form printing will have been fixed to the satisfaction of its users.
