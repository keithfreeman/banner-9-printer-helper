<?php

// define some basic settings for the UI
return [

    // relative url for the top nav logo and application name
    'app-home' => '/',

    // default title text for browser tab
    'app-title' => 'Banner 9 Printer Helper',

    // absolute url to the Chrome extension to be used with this application
    'chrome-extension' => 'https://chrome.google.com/webstore/',

    // relative url to the favicon
    'favicon' => '/white-label-ui/img/favicon.ico',

    // relative url to the small logo - 30px max height
    'logo' => '/white-label-ui/img/white-label-ui-logo-small.png',

    // relative url to the medium logo - 48px max height
    'logo-medium' => '/white-label-ui/img/white-label-ui-logo-medium.png',

    // non-production settings for the top bar
    'top-bar-non-production-main' => '#8B2145',
    'top-bar-non-production-border' => '#671931',

    // whether or not to display a page header
    'use-page-header' => true,
];