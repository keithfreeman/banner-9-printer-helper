<?php

namespace App\Http\Middleware;

use Closure;
use View;

class ShareUserInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            View::share('appUserId', auth()->user()->id);
            View::share('appUserName', auth()->user()->full_name);
        }
        return $next($request);
    }
}
