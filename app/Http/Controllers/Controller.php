<?php

namespace App\Http\Controllers;

use App\Hit;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incoming()
    {
        $title = rawurldecode(request('title'));

        $markup = rawurldecode(request('incoming'));

        $footer = rawurldecode(request('footer'));

        $stats = false;

        if ($title) {
            Hit::create(compact('title'));

            if (!preg_match('/[A-Z0-9]{7}\s+[\d\.]*(\s*\[[^\]]+\]\s+|\s*)\(([^\)]+)\)/', $title, $matches)) {
                $env = 'UNK';
            } else {
                $env = $matches[2];
            }

            $stats = [
                'total' => $env == 'UNK' ? 'UNK' : number_format(Hit::where('title', 'like', "%($env)%")->count()),
                'form' => number_format(Hit::where('title', $title)->count()),
            ];
        }

        return view('incoming', compact('title', 'markup', 'footer', 'stats'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function report(Request $request)
    {
        if ($request->has('message')) {
            $message = <<<EOH
Form:
{$request->get('b9Form')}

Message:
{$request->get('message')}

Contact:
{$request->get('contact')}
EOH;

            return mail(env('ERROR_EMAIL'), 'Banner 9 Printer Helper: Problem Reported', $message)
                ? response()->json(['success' => 'The report has been submitted.'])
                : response()->json(['error' => 'There was a problem submitting the report. Please try again.']);
        }

        return response()->json(['error' => 'No report was provided.  Nothing has been sent.']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function stats()
    {
        $stats = [];

        /** @var Collection $hits */
        $hits = Hit::all();

        $byEnvironment = $hits->groupBy(function($item, $key) {
            if (!preg_match('/[A-Z0-9]{7}\s+[\d\.]*(\s*\[[^\]]+\]\s+|\s*)\(([^\)]+)\)/', trim($item->title), $matches)) {
                return $item->title;
            }

            return $matches[2];
        })->map(function($group, $environment) {
            /** @var Collection $group */
            return [
                $environment,
                $group->unique('title')->count(),
                $group->count()
            ];
        });

        $stats[] = [
            'headers' => ['Environment', 'Unique Forms', 'Count'],
            'rows' => $byEnvironment
        ];

        $byDate = $hits->groupBy(function($item, $key) {
            return $item->created_at->toDateString();
        })->map(function($group, $date) {
            /** @var Collection $group */
            return [
                $date,
                $group->unique('title')->count(),
                $group->count()
            ];
        });

        $perPage = 10;

        if ($byDate->isNotEmpty()) {
            $rows = $byDate->reverse()->chunk($perPage)[request('page', 1) - 1];

            $paginator = new LengthAwarePaginator($rows, $byDate->count(), $perPage);
            $paginator->setPath('/stats');

            $stats[] = [
                'headers' => ['Date', 'Unique Forms', 'Count'],
                'rows' => $rows,
                'paginator' => $paginator,
                'paginated' => true,
            ];
        }

        $byTitle = $hits->groupBy('title')->map(function($group, $title) {
            /** @var Collection $group */
            $days = $group->groupBy(function($item) {
                return $item->created_at->toDateString();
            })->count();

            $usage = sprintf('%.2f', $group->count() / $days, 2);
            return [
                $title,
                "$usage/day over $days days",
                $group->count()
            ];
        });

        $stats[] = [
            'headers' => ['Form Designation', 'Usage', 'Count'],
            'rows' => $byTitle->sortByDesc(2)
        ];

        return view('stats', compact('stats'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statsAll()
    {
        $hits = Hit::orderBy('created_at')->paginate();

        return view('stats-all', compact('hits'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function testGrid()
    {
        return view('test.grid');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function testPresentation()
    {
        return view('test.presentation');
    }
}
