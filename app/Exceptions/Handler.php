<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use App\Breadcrumbs;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            return parent::render($request, $exception);
        }

        // Let's clear any breadcrumbs that may have been registered
        while (Breadcrumbs::pop()) {}

        Breadcrumbs::push('Home', 'home', route('home'));
        Breadcrumbs::push('Bad Juju', 'exclamation-circle');
        Breadcrumbs::setHeader("Well, that didn't work.");

        $debug = '';
        if (env('APP_DEBUG')) {
            $exception = $this->prepareException($exception);
            if ($exception instanceof HttpResponseException) {
                $debug = $exception->getResponse();
            } else {
                $debug = $this->prepareResponse($request, $exception)->getContent();
            }
        }

        return response()->view('errors.exception', compact('exception', 'debug'));
    }
}
