<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class AddUser extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a user to view the stats page';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        do {
            if (!empty($v)) {
                $this->output->error($v->messages()->all());
            }
            $name = $this->ask("Enter a username");
            $password1 = $this->secret("Enter a password");
            $password2 = $this->secret("Confirm password");
            $v = \Validator::make([
                'username' => $name,
                'password' => $password1,
                'password_confirmation' => $password2,
            ], [
                'username' => 'required|string|min:3',
                'password' => 'required|string|min:6|confirmed',
            ]);
        } while ($v->fails());

        $user = new User();
        $user->email = $name;
        $user->password = password_hash($password1, PASSWORD_BCRYPT);
        $user->save();
        $this->info('User was created. Stats can be seen at ' . route('stats') . '.');

        return 0;
    }
}
