<?php

namespace App;

class Breadcrumbs
{
    /** @var array */
    protected static $crumbs = [];

    /** @var string */
    protected static $header;

    /** @var string */
    protected static $subHeader;

    /** @var string */
    protected static $subTitle;

    /** @var string */
    protected static $title;

    /**
     * @return string
     */
    public static function getAppName()
    {
        $name = config('app.name');

        return $name != 'Laravel' ? $name : 'Awesome App (set in config/app.php -> name)';
    }

    /**
     * @return string
     */
    public static function getHeader()
    {
        // return one set by the developer
        if (!empty(static::$header)) {
            return static::$header;
        }

        // there are no crumbs so we return an empty string
        if (empty(static::$crumbs)) {
            return '';
        }

        // otherwise we use the last crumb
        return static::$crumbs[count(static::$crumbs) - 1][0];
    }

    /**
     * @return string
     */
    public static function getSubHeader()
    {
        // nothing if the subHeader is the same as the header
        return static::$subHeader == static::getHeader()
            ? ''
            : static::$subHeader;
    }

    /**
     * Determine the page subtitle to display in the browser tab.
     * The main page title is set in config directive 'app-title' in 'config/white-label-ui.php'
     * @return string
     */
    public static function getSubTitle()
    {
        // return one set by the developer
        if (!empty(static::$subTitle)) {
            return ': ' . static::$subTitle;
        }

        return empty(static::$crumbs)
            // if there are no crumbs we return nothing
            ? ''
            // otherwise, we will use the last crumb as the subTitle
            : ': ' . static::$crumbs[count(static::$crumbs) - 1][0];
    }

    /**
     * @return mixed|string
     */
    public static function getTitle()
    {
        return (!empty(static::$title) ? static::$subTitle : config('white-label-ui.app-title')) . static::getSubTitle();
    }

    /**
     * @return string|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function pageHeader()
    {
        $header = static::getHeader();
        $subHeader = static::getSubHeader();

        return empty($header) && empty($subHeader)
            ? ''
            : view('layouts.page-header', compact('header', 'subHeader'));
    }

    /**
     * @return mixed
     */
    public static function pop()
    {
        return array_pop(static::$crumbs);
    }

    /**
     * @param $name
     * @param null $icon
     * @param null $url
     */
    public static function push($name, $icon = null, $url = null)
    {
        static::$crumbs[] = func_get_args();
    }

    /**
     * @param $name
     * @param null $icon
     * @param null $url
     */
    public static function pushAsHeader($name, $icon = null, $url = null)
    {
        static::$header = $name;
        static::push($name, $icon, $url);
    }

    /**
     * @param $name
     * @param null $icon
     * @param null $url
     */
    public static function pushAsSubHeader($name, $icon = null, $url = null)
    {
        static::$subHeader = $name;
        static::push($name, $icon, $url);
    }

    /**
     * @return string|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function render()
    {
        return empty(static::$crumbs)
            ? ''
            : view('layouts.breadcrumbs', ['crumbs' => static::$crumbs]);
    }

    /**
     * @param $header
     */
    public static function setHeader($header)
    {
        static::$header = @(string) $header;
    }

    /**
     * @param $subHeader
     */
    public static function setSubHeader($subHeader)
    {
        static::$subHeader = @(string) $subHeader;
    }

    /**
     * @param $subTitle
     */
    public static function setSubTitle($subTitle)
    {
        static::$subTitle = @(string) $subTitle;
    }

    /**
     * @param $title
     */
    public static function setTitle($title)
    {
        static::$title = @(string) $title;
    }
}