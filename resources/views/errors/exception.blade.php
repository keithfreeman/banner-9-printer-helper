@extends('layouts.master-no-sidebar')

@php
    if (!empty($exception)) {
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
            switch ($exception->getStatusCode()) {
                case 404:
                    $error = 'The page you are looking for could not be found.';
                    break;
                default:
                    $error = 'HTTP Status Code ' . $exception->getStatusCode();
            }
        } else {
            $error = 'Fail: ' . $exception->getMessage();
        }
        $errors = new \Illuminate\Support\MessageBag([$error]);
    } elseif (empty($errors)) {
        $errors = new \Illuminate\Support\MessageBag(["I'm not even sure what 'that' was."]);
    }
@endphp

@section('content')
    {!! $debug !!}
@endsection