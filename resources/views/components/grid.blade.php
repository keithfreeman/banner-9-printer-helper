@if(false)
    <script type="text/javascript">
@endif

Vue.component('grid', {
    computed: {
        gridRows: function() {
            var lefts = $(this.markup).find('div.slick-pane-left:not(.slick-pane-header)').not('[style*="display: none"]').find('div.slick-row').get();
            var rights = $(this.markup).find('div.slick-pane-right:not(.slick-pane-header)').not('[style*="display: none"]').find('div.slick-row').get();
            var rows = [];
            do {
                var row = [];
                row.push(lefts.shift());
                row.push(rights.shift());
                rows.push(row);
            } while (lefts.length);
            return rows;
        },
        headersArray: function() {
            return $(this.markup).find('div.slick-header-columns div[role="columnheader"] > span:not(.slick-sort-indicator)');
        }
    },
    created: function() {
        var $this = this;
        this.headersArray.each(function(index, header) {
            $this.headers.push($(header).text().trim());
        });

        $.each(this.gridRows, function(rowIndex, row) {
            var columns = [];

            $this.gridColumns(row).each(function(colIndex, column) {
                if ($this.table.length == 0) {
                    $this.columnOrder.push(colIndex);
                }
                columns.push($this.gridColumnData(column));
            });

            $this.table.push(columns);
            $this.originalTable.push(columns);
        });
    },
    data: function() {
        return {
            columnOrder: [],
            headers: [],
            hidden: [],
            originalTable: [],
            sortColumn: {
                index : -1,
                direction: -1
            },
            table: []
        }
    },
    methods: {
        getSortIcon: function(index) {
            if (index != this.sortColumn.index) {
                return 'fa-sort grayed-out';
            }

            return this.sortColumn.direction == 1 ? 'fa-sort-amount-asc' : 'fa-sort-amount-desc';
        },
        gridColumnData: function(gridColumn) {
            var child = $(gridColumn).find(':first');
            if ($(child).hasClass('ui-text')) {
                return $(child).find('span')
                        ? $(child).text().trim()
                        : $(child).attr('title');
            }

            if ($(child).hasClass('ui-checkbox')) {
                return '<i class="fa fa-' +
                        ($(child).find('button').attr('aria-checked') == 'true' ? 'check-square' : 'square-o')
                        + '"></i>';
            }

            if ($(child).hasClass('editor-text')) {
                return $(child).val();
            }

            if ($(child).hasClass('ui-buttoninput')) {
                return $(child).find('input:first-child').val();
            }

            if ($(child).hasClass('ui-combobox')) {
                return $(child).find('select option[value="' + $(child).find('input').val() + '"]').text().trim();
            }

            return '';
        },
        gridColumns: function(gridRow) {
            return $(gridRow).find('div[role="gridcell"]');
        },
        isHidden: function(index) {
            return this.hidden.indexOf(index) != -1;
        },
        moveColumn: function(index, direction) {
            var curr = this.columnOrder.indexOf(index);
            this.$set(this.columnOrder, curr, this.columnOrder[curr + direction]);
            this.$set(this.columnOrder, curr + direction, index);
        },
        sortRows: function() {
            var $this = this;
            this.table = this.table.sort(function(a, b) {
                var index = $this.sortColumn.index;

                var x = a;
                var y = b;

                if ($this.sortColumn.direction == 2) {
                    x = b;
                    y = a;
                }

                if (x[index] > y[index]) {
                    return 1;
                }

                if (x[index] < y[index]) {
                    return -1;
                }

                return 0;
            });
        },
        toggleHidden: function(index) {
            if (this.isHidden(index)) {
                this.hidden.splice(this.hidden.indexOf(index), 1);
            } else {
                this.hidden.push(index);
            }
        },
        toggleSortColumn: function(index) {
            if (this.sortColumn.index != index) {
                this.sortColumn.index = index;
                this.sortColumn.direction = 1;
                this.sortRows();
            } else if (this.sortColumn.direction == 1) {
                this.sortColumn.direction = 2;
                this.sortRows();
            } else {
                this.sortColumn.index = -1;
                this.sortColumn.direction = -1;
                this.table = this.originalTable.slice(0);
            }
        },
        $: $
    },
    props: ['markup'],
    template: "{!! $template !!}"
});

@if(false)
    </script>
@endif