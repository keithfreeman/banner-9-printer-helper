@if(false)
    <script type="text/javascript">
@endif

Vue.component('presentation', {
    computed: {
        hasLegend: function() {
            return $(this.markup).closest('fieldset').length > 0;
        },
        legend: function() {
            return $(this.markup).closest('fieldset').find('legend').text().trim();
        },
        rows: function() {
            return $(this.markup).find('tr');
        }
    },
    created: function() {
        var $this = this;
        this.rows.each(function(index, row) {
            var actualRow = [];
            $this.columns(row).each(function(index, column) {
                var actualColumn = '';

                switch (true) {
                    case $this.isHidden(column):
                        break;

                    case $this.isSimple(column):
                        actualColumn = $this.getText(column);
                        break;

                    case $this.isGroup(column):
                        actualColumn = '<b>' + $this.getGroupLabel(column) + '</b>: ' + $this.getOptionList(column);
                        break;

                    case $this.isComplex(column):
                        var that = $this;
                        var added = [];
                        $this.complex(column).each(function(index, complexity) {
                            var label = that.getNonEmptyLabel(complexity);
                            if (added.indexOf(label) == -1) {
                                actualColumn += label + ' <span style="padding-right: 15px">' + that.getData(column, $(complexity)) + '</span>';
                                added.push(label);
                            }
                        });
                        break;

                    case $this.isLabeled(column):
                        actualColumn = $this.getLabel(column) + ' ' + $this.getData(column, $this.getLabelEl(column)) + ' ' + $this.getDescriptor(column);
                        break;

                    case $this.isTextField(column):
                        actualColumn = '<b>' + $(column).find('label').text().trim() + '</b>: ' + $(column).find('span').text().trim();
                        break;
                }

                actualRow.push(actualColumn);
            });
            $this.table.push(actualRow);
        });

        var maxColumns = 0;
        $.each(this.table, function(index, row) {
            maxColumns = Math.max(maxColumns, row.length);
        });

        $.each(this.table, function(index, row) {
            while (row.length < maxColumns) {
                $this.table[index].push('');
            }
        });
    },
    data: function() {
        return {
            hidden: false,
            table: []
        }
    },
    methods: {
        columnHasData: function(index) {
            var hasData = false;

            $.each(this.table, function(rowIndex, row) {
                $.each(row, function (columnIndex, column) {
                    if (columnIndex == index && column.length) {
                        hasData = true;
                    }
                });
            });

            return hasData;
        },
        columns: function(row) {
            return $(row).find('td');
        },
        complex: function(column) {
            return this.getLabelEl(column);
        },
        getData: function(column, label) {
            if (label.length == 0) {
                return '';
            }

            var data = $(column).find('input[name="' + label.attr('for').substr(1) + '"]').attr('title');
            if (data) {
                return data;
            }

            data = $(column).find('input[id="' + label.attr('for') + '"]').attr('title');
            if (data) {
                return data;
            }

            if (label.attr('for').match(/_btn$/)) {
                return '<i class="fa fa-' +
                        ($(column).find('button').attr('aria-checked') == 'true' ? 'check-square' : 'square-o')
                        + '"></i>';
            }

            return '';
        },
        getDescriptor: function(column) {
            var descriptor = $(column).find('div[data-widget="textfield"]');
            return descriptor.length
                    ? '<span style="padding-left: 15px">' + $(descriptor).find('span').text().trim() + '</span>'
                    : '';
        },
        getGroupLabel: function(column) {
            return $(column).find('div[data-widget="component"] > label:first-child').text().trim();
        },
        getLabel: function(column) {
            return this.getNonEmptyLabel(this.getLabelEl(column));
        },
        getLabelEl: function(column) {
            return $(column).find('label[for]');
        },
        getNonEmptyLabel: function(label) {
            label = $(label).text().trim();
            return label
                    ? '<b>' + label + '</b>:'
                    : '';
        },
        getOptionList: function(column) {
            var options = this.options(column);
            var list = [];

            options.each(function(index, option) {
                list.push(
                        '<span style="padding-right: 5px"><i class="fa fa-circle' + ($(option).find('button').attr('aria-checked') == 'true' ? '' : '-o') + '"></i> '
                        + $(option).find('label').text().trim()
                        + '</span>');
            });

            return list.join(' ');
        },
        getText: function(column) {
            return $(column).find('div[data-widget="text"]').text().trim();
        },
        isComplex: function(column) {
            return this.getLabelEl(column).length > 1;
        },
        isGroup: function(column) {
            return $(column).find('div[data-widget="radiogroup"]').length > 0;
        },
        isHidden: function(column) {
            return $(column).find('div[style*="display: none"]').length > 0;
        },
        isLabeled: function(column) {
            return this.getLabelEl(column).length > 0;
        },
        isSimple: function(column) {
            return $(column).find('div[data-widget="text"]').length > 0;
        },
        isTextField: function(column) {
            return $(column).find('div[data-widget="textfield"]').length > 0;
        },
        options: function(column) {
            return $(column).find('div[data-widget="radiobox"]');
        },
        rowHasData: function(r) {
            return r.join('').length;
        },
        $: $
    },
    props: ['markup'],
    template: "{!! $template !!}"
});

@if(false)
    </script>
@endif