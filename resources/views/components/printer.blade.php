@if(false)
<script type="text/javascript">
@endif

Vue.component('banner-printer', {
    created: function() {
        this.message = this.messageTemplate;
    },
    computed: {
        activities: function() {
            var activities = [];

            var pieces = $(this.footer).find('div[id^="activity"][style!="display: none;"]');
            
            pieces.each(function(index, piece) {
                if ($(piece).find('label').length && $(piece).find('span').length) {
                    activities.push(piece);
                }
            });

            return activities;
        },
        criteria: function() {
            var criteria = {};

            var pieces = $(this.doc).find('div.layout-row label').not('.empty');

            pieces.each(function(index, piece) {
                if ($(piece).parent().css('display') == 'none') {
                    return;
                }

                var value = '';

                var data = $(piece).closest('div[data-widget="component"]').find('span.ui-widget-content');
                if (!data.length) {
                    data = $(piece).closest('div[data-widget="datefield"]').find('span.ui-widget-content');
                }
                if (!data.length) {
                    data = $(piece).closest('div[data-widget="textinput"]').find('span.ui-widget-content');
                }
                if (!data.length) {
                    data = $(piece).closest('div[data-widget="combobox"]').find('span.ui-widget-content');
                }

                data.each(function(index, datum) {
                    value += ' ' + $(datum).text().trim();
                });

                if (!data.length) {
                    data = $(piece).closest('div[data-widget="component"]').find('div[data-widget="checkbox"] > button');
                    if (data.length) {
                        value = '<i class="fa fa-' +
                                ($(data).attr('aria-checked') == 'true' ? 'check-square' : 'square-o') +
                                '"></i>'
                    }
                }

                criteria[$(piece).text().trim()] = value;
            });

            return criteria;
        },
        messageTemplate: function() {
            return "ENTER DETAILS HERE\n(missing/wrong data, malformed tables, etc.)";
        },
        sections: function() {
            var sections = [];
            var pieces = $(this.doc).find('div[data-widget="collapsiblepanel"] h3.ui-collapsiblepanel-header');
            pieces.each(function(index, piece) {
                if ($(piece).closest('div.ui-tabs-hide').length == 0) {
                    sections.push(piece);
                }
            });

            pieces = $(this.doc).find('div[data-widget="accordion"] h3.ui-accordion-header');
            pieces.each(function(index, piece) {
                if ($(piece).closest('div.ui-tabs-hide').length == 0) {
                    sections.push(piece);
                }
            });

            return sections;
        }
    },
    data: function() {
        return {
            b9Form: this.title,
            buttonLabel: 'Report a problem',
            contact: '',
            doc: markup,
            error: '',
            footer: footer,
            message: '',
            success: ''
        };
    },
    methods: {
        done: function(response) {
            this.buttonLabel = 'Report a problem';
            if (response.error != undefined) {
                this.error = response.error;
            } else if (response.success != undefined) {
                this.success = response.success;
            } else {
                this.error = 'There was an error submitting the report. Please try again.';
            }
        },
        fail: function(response) {
            this.buttonLabel = 'Report a problem';
            if (response.error != undefined) {
                this.error = response.error;
            } else {
                this.error = 'There was an error submitting the report. Please try again.';
            }
        },
        grids: function(section) {
            return $(section).parent().find('div[data-widget="datagrid"]').get();
        },
        resetResult: function() {
            this.error = '';
            this.success = '';
        },
        reportProblem: function() {
            this.buttonLabel = 'Sending...';
            $.ajax({
                type: 'POST',
                url: '{{ route('report') }}',
                data: {
                    b9Form: this.b9Form,
                    contact: this.contact,
                    message: this.message,
                    _token: '{{ csrf_token() }}'
                }
            })
                    .done(this.done)
                    .fail(this.fail);
        },
        sectionTitle: function(section) {
            var titleDiv = $(section).find('div.ui-collapsiblepanel-title');
            if (titleDiv.length) {
                return titleDiv.text().trim();
            }

            return $(section).text().trim();
        },
        tables: function(section) {
            return $(section).parent().find('table[role="presentation"]').filter(function(index, table) {
                return $(table).closest('div[style*="display: none"]').length == 0;
            }).get();
        },
        $ : $
    },
    props: ['stats', 'title'],
    template: "{!! $template !!}"
});

@if(false)
    </script>
@endif
