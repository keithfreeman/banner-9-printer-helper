<div>
    <div v-if="error || success" class="alert no-print" :class="error ? 'alert-danger' : 'alert-success'" role="alert" style="margin-top: 20px">
        <button type="button" class="close" aria-label="Close" @click="resetResult()">
            <span aria-hidden="true"><i class="fa fa-times-circle"></i></span>
        </button>
        @{{ error + success }}
    </div>
    <div class="pull-right no-print">
        <span v-if="stats"><b>Print Counts</b> [This Form: @{{ stats.form }}]<span v-if="stats.total != 'UNK'"> [All Forms: @{{ stats.total }}]</span></span>
        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#problem">@{{ buttonLabel }}</button>
    </div>
    <h4><b>@{{ title }}</b></h4>
    <ul class="list-unstyled clearfix">
        <li v-for="(value, criterion) in criteria" class="pull-left" style="padding-right: 15px;">
            <b v-html="criterion"></b>: <span v-html="value"></span>
        </li>
    </ul>
    <fieldset v-for="section in sections">
        <legend>@{{ sectionTitle(section) }}</legend>
        <presentation v-for="(table, index) in tables(section)" :markup="table" :key="index"></presentation>
        <grid v-for="(grid, index) in grids(section)" :markup="grid" :key="index"></grid>
    </fieldset>
    <fieldset v-if="activities.length">
        <legend>Activity Details</legend>
        <ul class="list-unstyled">
            <li v-for="activity in activities">
                <b>@{{ $(activity).find('label').text().trim() }}</b>: @{{ $(activity).find('span').text().trim() }}
            </li>
        </ul>
    </fieldset>

    <div class="modal fade no-print" id="problem" tabindex="-1" role="dialog" aria-labelledby="problemLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i></button>
                    <h4 class="modal-title" id="problemLabel">Report a problem</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        This is to facilitate reporting any issues found with data display or page printing.  Some basic information about the form you are using has been filled in already.  Please edit the message and submit it to the development team for review.
                    </div>
                    <form>
                        <div class="form-group">
                            <label for="b9form">I have a problem with this form:</label>
                            <input class="form-control" id="b9form" type="text" v-model="b9Form" />
                        </div>
                        <div class="form-group">
                            <label for="message">Tell us about the problem:</label>
                            <textarea class="form-control" id="message" rows="10" v-model="message"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="contact">For additional info, please contact me at:</label>
                            <input class="form-control" id="contact" type="text" v-model="contact" placeholder="e.g. Access ID/phone number/etc."/>
                        </div>
                        <div class="form-group clearfix">
                            <button class="btn btn-primary pull-right" data-dismiss="modal" style="margin-left: 15px" @click.prevent="reportProblem()">Submit</button>
                            <button class="btn btn-warning pull-right" style="margin-left: 15px" @click.prevent.stop="message = messageTemplate">Reset</button>
                            <button class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
