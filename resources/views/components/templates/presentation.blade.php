<div class="panel panel-default avoid-break" :class="hidden ? 'grayed-out' : ''">
    <div v-if="hasLegend" class="panel-heading">
        <h3 class="panel-title" :class="hidden ? 'grayed-out' : ''">
            @{{ legend }}
            <i class="fa fa-print text-success" :class="hidden ? 'grayed-out' : ''" @click="hidden = !hidden" title="Toggle Panel Printing"></i>
        </h3>
    </div>
    <table class="table table-bordered avoid-break">
        <tbody>
            <tr v-for="(row, rowIndex) in table" v-if="rowHasData(row)">
                <td v-for="(column, columnIndex) in row" v-if="columnHasData(columnIndex)" v-html="column"></td>
            </tr>
        </tbody>
    </table>
</div>
