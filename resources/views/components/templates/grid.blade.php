<div>
    <h4 v-if="columnOrder.length > 14" class="no-print">
        <i class="fa fa-info-circle text-info"></i>
        <span v-if="columnOrder.length < 17">
            The grid below has @{{ columnOrder.length }} columns. You might consider hiding some columns, printing this in landscape mode, or using the Scale feature.
        </span>
        <span v-else>
            The grid below has @{{ columnOrder.length }} columns. You might consider hiding some columns or using the Scale feature when printing.
        </span>
    </h4>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th v-for="(columnIndex, index) in columnOrder" class="grid-header" :class="isHidden(columnIndex) ? 'grayed-out' : ''">
                    @{{ headers[columnIndex] }}
                    <div>
                        <i v-if="index" class="action fa fa-arrow-left pull-left" @click="moveColumn(columnIndex, -1)" title="Move Column Left"></i>
                        <i v-if="table.length > 2" class="action fa" :class="getSortIcon(columnIndex)" @click="toggleSortColumn(columnIndex)" title="Change Column Sort"></i>
                        <i class="action fa fa-print" :class="[isHidden(columnIndex) ? '' : 'text-success']" @click="toggleHidden(columnIndex)" title="Toggle Column Printing"></i>
                        <i v-if="index != columnOrder.length -1" class="action fa fa-arrow-right pull-right" @click="moveColumn(columnIndex, 1)" title="Move Column Right"></i>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="row in table">
                <td v-for="columnIndex in columnOrder" v-html="row[columnIndex]" :class="isHidden(columnIndex) ? 'grayed-out' : ''"></td>
            </tr>
        </tbody>
    </table>
</div>