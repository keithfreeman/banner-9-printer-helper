@extends('layouts.master')

@section('main')
    <div class="container-fluid white-label-ui-main white-label-ui-without-sidebar">
        {!! \App\Breadcrumbs::pageHeader() !!}
        {!! \App\Breadcrumbs::render() !!}
        @yield('main-content')
    </div>
@endsection