<div class="page-header white-label-ui-page-header">
    <h1>
        {{-- If for some reason, somebody defined a subheader but no header, we will fix what must be a mistake. --}}
        {{ $header ?: $subHeader }}
        {{-- We only show the subheader if it is set and is not the same thing as the header. --}}
        @if(!empty($subHeader) && $subHeader != $header)
            <small>{{ $subHeader }}</small>
        @endif
    </h1>
</div>