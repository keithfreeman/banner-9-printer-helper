<ol class="breadcrumb">
    @while($crumb = array_shift($crumbs))
        {{-- If $crumbs is empty, we are at the last crumb. This should get the active class added. --}}
        <li @if(empty($crumbs)) class="active" @endif>
            {{-- If there is an icon defined, let's show it. --}}
            @if(!empty($crumb[1]))
                <i class="fa fa-{{ $crumb[1] }}"></i>
            @endif
            {{-- If there is no URL provided, or we are on the last crumbe, we don't use a link. --}}
            @if(empty($crumb[2]) || empty($crumbs))
                {{ $crumb[0] }}
            {{-- Otherwise, we create a link with URL. --}}
            @else
                <a href="{{ $crumb[2] }}">
                    {{ $crumb[0] }}
                </a>
            @endif
        </li>
    @endwhile
</ol>