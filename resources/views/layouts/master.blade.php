<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{!! cacheBustingUrl(config('white-label-ui.favicon')) !!}">

    <title>{{ \App\Breadcrumbs::getTitle() }}</title>

    <link href="{!! cacheBustingUrl('/white-label-ui/css/bootstrap-plugins.min.css') !!}" rel="stylesheet">
    <link href="{!! cacheBustingUrl('/white-label-ui/css/white-label-ui-bootstrap.css') !!}" rel="stylesheet">
    <link href="{!! cacheBustingUrl('/white-label-ui/css/white-label-ui.css') !!}" rel="stylesheet">
    <link href="{!! cacheBustingUrl('/white-label-ui/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
    @include('layouts.partials.html-head')

    @stack('html-head')
</head>
<body>

<div class="navbar navbar-default navbar-fixed-top"
     @if (app()->environment() != 'production')
        style="background: {{ config('white-label-ui.top-bar-non-production-main') }}; border-bottom: 3px solid {{ config('white-label-ui.top-bar-non-production-border') }}"
     @endif
     role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-logo pull-left" href="{{ config('white-label-ui.app-home') }}">
                <img src="{!! cacheBustingUrl(config('white-label-ui.logo')) !!}" alt="{{ config('white-label-ui.app-title') }}" height="30">
            </a>
            <a class="navbar-brand" href="{{ config('white-label-ui.app-home') }}">{{ \App\Breadcrumbs::getAppName() }}</a>
        </div>
        <div class="navbar-collapse collapse" style="height: 1px;">
            <div class="navbar-text navbar-right hidden-xs" title="{{ $appUserName or '$appUserName not set' }}"><i class="fa fa-user fa-lg"></i> {{ $appUserId or '$appUserId not set' }}</div>
            <p class="navbar-text visible-xs"><i class="fa fa-user"></i> {{ $appUserId or '$appUserId not set' }}: {{ $appUserName or '$appUserName not set' }}</p>
            <ul class="nav navbar-nav visible-xs">
                @include('layouts.partials.sidebar-nav')
            </ul>
        </div>
    </div>
</div>

@yield('main')

<!-- Bootstrap core JavaScript
==================================================
 Placed at the end of the document so the pages load faster -->
<script src="{!! cacheBustingUrl('/white-label-ui/js/jquery.js') !!}"></script>
<script src="{!! cacheBustingUrl('/white-label-ui/js/bootstrap.min.js') !!}"></script>
<script src="{!! cacheBustingUrl('/white-label-ui/js/bootstrap-select.min.js') !!}"></script>
@include('layouts.partials.body-footer')

@stack('body-footer')

</body>
</html>