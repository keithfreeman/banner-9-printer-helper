@extends('layouts.without-sidebar')

@section('main-content')
    @include('layouts.errors-successes')
    @yield('content')
@endsection
