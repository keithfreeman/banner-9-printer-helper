@extends('layouts.master-no-sidebar')

@section('content')
    <div class="container">
        <h3 class="text-primary">Printing Statistics - by group<a class="pull-right" href="{{ route('stats.all') }}"><i class="fa fa-search"></i></a></h3>
        @foreach($stats as $stat)
            <h4>By {{ $stat['headers'][0] }}</h4>
            @if(!empty($stat['paginated']))
                {{ $stat['paginator']->links() }}
            @endif
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        @foreach($stat['headers'] as $header)
                            <th>{{ $header }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @if($stat['rows']->isEmpty())
                        <tr><td colspan="{{ count($stat['headers']) }}">{none yet}</td></tr>
                    @else
                        @foreach($stat['rows'] as $row)
                            <tr>
                                @foreach($row as $column)
                                    <td>{{ $column }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            @if(!empty($stat['paginated']))
                {{ $stat['paginator']->links() }}
            @endif
            <hr/>
        @endforeach
    </div>
@endsection