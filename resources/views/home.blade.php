@extends('layouts.master-no-sidebar')

@section('content')
    <h1 class="text-primary text-center"><img src="{{ cacheBustingUrl(config('white-label-ui.logo-medium')) }}" /> Help is Here!</h1>
    <div class="well well-sm text-center">
        <h3>Use the Banner 9 Printer Helper extension to print Forms from Banner 9.</h3>
        <p>This helper requires a browser extension to work.  For Chrome, click <a href="{{ config('white-label-ui.chrome-extension') }}">here</a>.</p>
    </div>
@endsection