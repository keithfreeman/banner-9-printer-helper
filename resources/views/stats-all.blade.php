@extends('layouts.master-no-sidebar')

@section('content')
    <div class="container">
        <h3 class="text-primary">Printing Statistics - all hits <a class="pull-right" href="{{ route('stats') }}"><i class="fa fa-line-chart"></i></a></h3>
        <div>{{ $hits->links() }}</div>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Form Designation</th>
                    <th>Timestamp</th>
                </tr>
            </thead>
            <tbody>
            @foreach($hits as $hit)
                <tr>
                    <td>{{ $hit->title }}</td>
                    <td>{{ $hit->created_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>{{ $hits->links() }}</div>
    </div>
@endsection