@extends('layouts.master-no-sidebar')

@push('body-footer')
    <script type="text/javascript">
        var markup =  {!! json_encode($markup) !!};

        var footer = {!! json_encode($footer) !!};

        var activeTab = $(markup).find('li.ui-state-active a').attr('href').substr(1);
        markup = $(markup).find('div[id="' + activeTab + '"]');

        footer = $(footer).find('li#activity');

        new Vue({el: '#app'})
    </script>
@endpush

@section('content')
    <div id="app">
        <banner-printer title="{{ $title or 'No Title' }}" :stats="{{ json_encode($stats) }}"></banner-printer>
    </div>
@endsection