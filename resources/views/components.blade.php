{{-- Oh no, you didn't!  Oh yes, I did. --}}
@foreach (glob(resource_path('views/components/*.blade.php')) as $view)
    @php
        $view = explode('/', $view);
        $view = array_pop($view);;
        $view = str_replace('.blade.php', '', $view);
    @endphp
    @include('components.' . $view, ['template' => processTemplate('components.templates.' . $view)])
@endforeach