<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@home')->name('home');
Route::middleware('auth.basic')->get('/stats', 'Controller@stats')->name('stats');
Route::middleware('auth.basic')->get('/stats/all', 'Controller@statsAll')->name('stats.all');
Route::post('/print', 'Controller@incoming');
Route::post('/report', 'Controller@report')->name('report');

if (app()->environment() != 'production') {
    Route::get('/test/grid', 'Controller@testGrid')
        ->name('test.grid');
    Route::get('/test/presentation', 'Controller@testPresentation')
        ->name('test.presentation');
}

// Cheap way to get all of the Vue components to the browser because
// I'm too lazy to pre-compile assets
Route::get('/assets/js/components.js', function() {
    return view('components');
});